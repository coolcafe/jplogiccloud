[![Fork me on Gitee](https://gitee.com/romantichjw/jplogiccloud/widgets/widget_3.svg)](https://gitee.com/romantichjw/jplogiccloud)
[![star](https://gitee.com/romantichjw/jplogiccloud/badge/star.svg?theme=dark)](https://gitee.com/romantichjw/jplogiccloud/stargazers)
[![fork](https://gitee.com/romantichjw/jplogiccloud/badge/fork.svg?theme=dark)](https://gitee.com/romantichjw/jplogiccloud/members)

# jplogiccloud微服务架构平台-微服务快速开发脚手架

**项目介绍**

**一、功能点：**

   >该项目主要是 **RBAC权限管理系统实例** 内含功能"麻雀虽小五脏俱全"，可以基于平台组件拓展很多产品应用场景的实现（ **其中涵盖微服务架构（监控+分布式配置+服务熔断+服务资源隔离+服务限流+反爬限制+多级缓存架构等）+大数据（实时计算与存储）+多种对象存储方案 + 爬虫引擎（多场景数据抓取）+搜索引擎（海量数据搜索）+ 热点数据预测 +ELK+APM(Skywalking)+GPE+Docker+K8S+CICD+第三方服务等的综合实例** ），基于Springcloud企业级微服务架构的快速开发脚手架，除了能够快速的构建权限管理系统之外同时也能够 **快速基于该脚手架快速构建企业级产品（互联网APP、SAAS系统等）** ，并提供相关Springcloud微服务架构运维监控组件（Zipkin\Turbine\Hystrix-dashboard\Spring boot admin\Nacos\Sentinel\分布式Swagger在线文档）以及集成了主流的APM监控运维体系Skywalking(对标springcloud的ZipkinServer)、集成了GPE（Grafana +Prometheus+Exporters）系统监控系统，同时也集成了ELK日志收集平台，能够把一整套目前互联网公司主流技术栈全套迁移到各个企业中去，并根据需要灵活定制，同时提供一整套企业级CI/CD解决方案，支持Docker/K8s服务容器化自动化。
	
**二、技术点：**

   >该脚手架集成了丰富的Spring-Boot-Starter，可根据企业的产品功能的需要灵活装配需要的组件，这让开发人员花更多的时间在使用这些集成好的组件完成具体的业务逻辑，并能够通过平台提供的各种监控体系构件很清楚的了解系统服务生命周期过程中的各种指标状态，基本上采用了开源技术实现以及自研组件，因此基于微服务架构开源标准，目前的开发人员很容易上手使用，可以为互联网创业公司解决微服务架构相关问题，可以帮助公司快速构建项目。本项目由多个后端Springcloud微服务项目和1个前端web项目共同组成。实现了基于RBAC的权限管理解决方案，还可以基于该平台构建各种互联网公司的app服务端微服务架构以及认证体系等。
   
   
* 核心框架：Greenwich.SR5全家桶
* 安全框架：Spring Security + Springcloud Oauth2
* 事件驱动EDA框架：Akka-standalone模式、Akka-cluster模式
* 分布式搜素引擎框架：ElasteSearch 
* 腾讯IM SDK：IM starter
* 分布式任务调度：Elastic Job Lite + XXL-JOB
* 持久层框架：MyBatis（myBatis-generator）、Sharding-jdbc、Mongo
* 数据库连接池：Alibaba Druid
* 日志管理：ELK(Log-pilot)
* OSS文件存储：OSS-Aliyun、OSS-fastdfs、OSS-minio、OSS-qiniucloud、OSS-Tencant (按照公司技术选型要求灵活集成)
* 监控体系：GPE(Grafana + Prometheus + Exporters(Sofa-lookout监控[基于eureka服务发现]等各种Exporter))
* 分布式服务调用链跟踪服务以及APM：ZinkinServer + Skywalking(以及微服务docker镜像集成skywaling_agent) 
* 前端框架：bootstrap以及其他第三方开源前端插件
* 第三方组件服务： 邮件服务、百度地图API
* 消息队列：Kafka + Rabbitmq + Rocketmq 
* 大数据存储：Hbase
* 反爬虫、接口防盗刷组件：反爬虫、接口防盗刷检查，支持多种反爬规则检查
* Ocr实名认证：阿里云Ocr服务（Ocr-starter）
* OCR图像文字识别
* 消息推送服务：极光推送
* 短信服务：阿里云短信服务 +　第三方短信服务　短信服务路由
* 服务降级(服务保护、资源隔离)：Alibaba-Sentinel＋Springcloud-Hystrix
* 分布式事务：Alibaba-Seata ＋ LCN + MQ消息最终一致性事务控制
* CI/CD：GIT + Gitlab + Nexus + Maven + Jenkins Pipline + Sonar + Harbor + Docker + K8s 
* 分布式配置：Apollo + Nacos
* 分库分表：Sharding-jdbc- + Mycat
* 消息推送：WebSocket
* 热点数据预测

**三、功能简述：**

* 集成了Oauth2多种认证方式
（手机号、openId、图形验证码、第三方系统授权登陆，如微信开发平台、支付宝等）以及认证模式；
* 服务注册发现 
* 路由与负载均衡
* 服务降级与熔断
* 服务资源反爬虫验证
* 支持多种分布式配置中心
* 支持多种日志平台
* 缓存+lua并发支持 
* 支持多种分布式锁
* 分布式任务调度
* 分布式事务管理
* 应用命令行
* 服务协调以及目录命名服务
* 支持CI/CD持续集成
* 分布式高性能Id生成器（大厂使用的方案）
* 系统监控基础设施
* 服务调用链监控
* 微服务应用监控
* 慢查询SQL监控
* 服务限流监控
* RBAC权限管理
* 数据库分库分表
* 聚合的Swagger接口文档
* IM聊天模块
* 搜索引擎
* 经纬坐标翻译
* 支持多种分布式文件存储
* 事件驱动
* 容器化部署
* 自研爬虫引擎J2crawler
* OCR图像文字识别
* 微信公众号
* Storm实时流计算
* 热点数据预测

**四、架构图：**

脚手架通用架构（架构可视化，第三方动态架构可视化工具）：

![架构图](http://image.jplogic.cn/img/Jplogiccloud-archecture.jpg "架构图")

J2crawler爬虫引擎架构图：

![J2crawler爬虫引擎架构图](http://image.jplogic.cn/img/J2crawler-archecture.png "J2crawler爬虫引擎架构图")

J2crawler爬虫引擎内部组件架构图：

![J2crawler爬虫引擎内部组件架构图](http://image.jplogic.cn/img/jplogiccloud-j2crawler-engine.png "J2crawler爬虫引擎内部组件架构图")

Java微信公众号文章抓取实例：
![J2crawler爬虫微信公众号文章抓取实例架构图](http://image.jplogic.cn/img/wechat_jiagou.jpg "J2crawler爬虫微信公众号文章抓取实例架构图")

微信公众号文章热词（词频）实时计算并刷新词云图：
PS:该案例可运用到其他产品的场景，如电商热门商品预热、广告实时点击量、APP界面用户行为热力图等实时流计算的场景

![实时流计算](http://image.jplogic.cn/img/storm1.png "实时流计算")
![实时流计算](http://image.jplogic.cn/img/storm2.png "实时流计算")
![实时流计算](http://image.jplogic.cn/img/storm3.png "实时流计算")
![实时流计算](http://image.jplogic.cn/img/storm4.png "实时流计算")
![实时流计算](http://image.jplogic.cn/img/storm5.png "实时流计算")

抓取的公众号文章展示：

<div>
<img src="http://image.jplogic.cn/img/wechat_mp_1.jpg" alt="微信公众号文章抓取" width="290" height="600" align="left" />
<img src="http://image.jplogic.cn/img/wechat_mp_2.jpg" alt="微信公众号文章抓取" width="290" height="600" align="left" />
<img src="http://image.jplogic.cn/img/wechat_mp_3.jpg" alt="微信公众号文章抓取" width="290" height="600" align="left" />
</div>


jplogic大数据BI大屏展示：

![数据BI大屏](http://image.jplogic.cn/img/bishow.png "数据BI大屏")

**五、项目源代码结构：**


├── docs   
│   ├── databases  -- 数据相关脚本目录   
│   └── scripts  -- k8s、docker、docker-compose、shell各种环境脚本CICD等以及自定义docker镜像        
├── jplogiccloud-apps   -- jplogicloud 应用集合    
│   ├── jplogiccloud-app-crawler   -- 博客、微信公众号、代理ip、疫情、舆情、图库等场景的爬虫微服务支持集群抓取    
│   ├── jplogiccloud-app-dfs  -- 通用的分布式文件（对象）管理微服务，支持海量文件（图片、文件等）存储以及管理    
│   ├── jplogiccloud-app-keygen   -- 加密秘钥对生成器以及类telnet命令行访问微服务工具    
│   ├── jplogiccloud-app-platmgr  -- RBAC权限管理系统实例       
│   ├── jplogiccloud-app-storm  -- 实时流计算（用户行为分析-微信公众号文章热点词分析、热点数据实时流计算）            
│   ├── jplogiccloud-app-wechat-mp -- 微信公众号微服务           
│   └── pom.xml    
├── jplogiccloud-bootstarter  -- 脚手架通用springboot组件starter自定义组件    
│   ├── jplogiccloud-bootstarter.iml    
│   ├── jplogiccloud-starter-akka-cluster  -- akka集群模式支持以及Akka事件驱动的RPC封装和以及Akka应用监控等         
│   ├── jplogiccloud-starter-akka-standalone  -- akkas-standalone模式，轻量级的事件驱动变成     
│   ├── jplogiccloud-starter-apollo  --  分布式配置中心集成组件     
│   ├── jplogiccloud-starter-ehcache  -- 多级缓存之本地缓存        
│   ├── jplogiccloud-starter-email  -- 邮件相关  
│   ├── jplogiccloud-starter-es   -- ElasticSearch相关搜索组件封装      
│   ├── jplogiccloud-starter-etcd  -- 类zk式分布式K-V 高可用强一致性数据存储系统组件，主要用于自定义服务发现以及源数据存储     
│   ├── jplogiccloud-starter-fraudcheck  -- 微服务反爬虫组件 （支持多种反爬策略，可根据应用场景灵活装配）    
│   ├── jplogiccloud-starter-geo  -- 地理位置翻译相关     
│   ├── jplogiccloud-starter-hystrix  -- 服务资源隔离、熔断、降级，防止高并发场景下的各种"服务雪崩"效应       
│   ├── jplogiccloud-starter-hbase  -- 关于hbase的通用的操作封装     
│   ├── jplogiccloud-starter-im-tencent  -- 腾讯im-SDK封装操作       
│   ├── jplogiccloud-starter-j2crawler  -- 自封装的分布式微服务java爬虫引擎，可基于它抓取各种场景下的互联网数据        
│   ├── jplogiccloud-starter-logger -- 微服务LogStash日志采集拦截、调用轨迹封装组件        
│   ├── jplogiccloud-starter-mq-kafka  -- kafka相关通用操作，支持大数据包的拆包\组包操作等      
│   ├── jplogiccloud-starter-mq-rabbit -- 关于rabbitmq相关分布式事务相关封装      
│   ├── jplogiccloud-starter-netty  --  分布式网络编程相关，集成etcd等实现多节点微服务热点数据计算预测等场景          
│   ├── jplogiccloud-starter-oauth2-feign  -- 声明式的springcloud的restful rpc调用    
│   ├── jplogiccloud-starter-ocr-alicloud -- 阿里云OCR-SDK人脸识别封装       
│   ├── jplogiccloud-starter-ocr-tesseract -- 图像文字识别，可用于二维码等图片文字识别场景等     
│   ├── jplogiccloud-starter-oss-alicloud -- 阿里云对象存储封装      
│   ├── jplogiccloud-starter-oss-base  -- 对象存储通用封装      
│   ├── jplogiccloud-starter-oss-fastdfs  -- fastdfs分布式文件（对象）组件封装      
│   ├── jplogiccloud-starter-oss-minio  -- 轻量级minio对象存储组件封装      
│   ├── jplogiccloud-starter-oss-qiniucloud  -- 青牛云对象存储组件       
│   ├── jplogiccloud-starter-oss-tencent -- 腾讯云对象存储组件封装    
│   ├── jplogiccloud-starter-prometheus -- 微服务prometheus监控探针封装    
│   ├── jplogiccloud-starter-redis  -- 分布式缓存通用、分布式锁、bloomFilter、限流等操作  
│   ├── jplogiccloud-starter-sentinel  -- alibaba-sentinel服务保护、资源隔离、降级、熔断集成  
│   ├── jplogiccloud-starter-sms-alicloud  -- 阿里云SMS服务通用封装    
│   ├── jplogiccloud-starter-sqllog  -- 慢SQL监控拦截      
│   ├── jplogiccloud-starter-websocket -- websocker通用封装     
│   ├── jplogiccloud-starter-tenant -- saas多租户封装（1、独立数据库、2、共享数据库、独立 Schema 3、共享数据库、共享 Schema、共享数据表）     
│   ├── jplogiccloud-starter-zookeeper  -- 关于zookeeper的通用操作封装以及分布式锁等操作       
│   └── pom.xml    
├── jplogiccloud-common  
│   ├── jplogiccloud-common-core  -- 脚手架通用Common封装    
│   ├── jplogiccloud-common.iml  
│   ├── jplogiccloud-common-security -- 脚手架安全框架封装支持oauth2-token     
│   └── pom.xml  
├── jplogiccloud-component  
│   ├── jplogiccloud-component.iml  
│   ├── jplogiccloud-component-tracer  -- 微服务分布式调用规则跟踪拦截集成组件      
│   ├── jplogiccloud-component-ws  -- websocket相关组件封装     
│   └── pom.xml  
├── jplogiccloud-dao  
│   ├── jplogiccloud-dao-base  -- 数据访问层通用封装        
│   ├── jplogiccloud-dao-crawler  -- 爬虫中心数据访问层，支持分库分表       
│   ├── jplogiccloud-dao-file  -- 文件中心数据访问层，支持分库分表         
│   ├── jplogiccloud-dao.iml  
│   ├── jplogiccloud-dao-lgc  -- 快照日志中心数据访问层，支持分库分表          
│   ├── jplogiccloud-dao-sys  -- 系统中心数据访问层，支持分库分表         
│   └── pom.xml  
├── jplogiccloud-environment  
│   ├── jplogiccloud-admin-server  -- springboot微服务管理控制台       
│   ├── jplogiccloud-config-server  -- springcloud config 配置中心       
│   ├── jplogiccloud-environment.iml  
│   ├── jplogiccloud-monitor-dashboard  -- 监控hystrix + turbine 之dashboard     
│   ├── jplogiccloud-monitor-prometheus-grafana  -- 微服务监控prometheus-grafana     
│   ├── jplogiccloud-monitor-skywalking  -- APM应用监控平台之skywalking        
│   ├── jplogiccloud-monitor-zipkin --  分布式调用轨迹跟踪zipkin        
│   ├── jplogiccloud-registrycenter-eureka  -- 注册中心         
│   ├── jplogiccloud-registrycenter-nacos  -- 注册中心以及配置中心        
│   ├── jplogiccloud-sentinel-dashboard  -- sentinel管理控制台      
│   └── pom.xml  
├── jplogiccloud-gateway  -- 微服务网关     
│   ├── build-image.sh  
│   ├── Dockerfile  
│   ├── jplogiccloud-gateway.iml  
│   ├── pom.xml  
│   └── src  
├── jplogiccloud-master.iml  
├── package-dev.bat  
├── package-dev.sh  
├── package-pro.bat  
├── package-pro.sh  
├── package-test.bat  
├── package-test.sh  
├── pom.xml  
└── README.md   



**作者介绍**

开源技术爱好者，springboot\springcloud\dubbo\大数据相关技术（spark\hadoop\kafka\scala）\容器docker等主流开源框架实践者，非常欢迎与大家进行相关交流，我的csdn博客地址:https://blog.csdn.net/romantichjwhjwhjw/ , 我的github地址：https://gitee.com/romantichjw;

**项目在线体验**

- 体验链接：<https://www.jplogic.cn/login>   登陆账号admin/huangjianwen

**项目CI/CD可持续集成演示**

![cicd](http://image.jplogic.cn/img/show-gitlab.png "cicd")
![cicd](http://image.jplogic.cn/img/show-sonar.png "cicd")
![cicd](http://image.jplogic.cn/img/show-jenkins.png "cicd")
![cicd](http://image.jplogic.cn/img/show-harbor.png "cicd")

**项目GPE监控预警演示**

![monitor](http://image.jplogic.cn/img/show-grafana1.png "monitor")
![monitor](http://image.jplogic.cn/img/show-grafana2.png "monitor")
![monitor](http://image.jplogic.cn/img/show-grafana3.png "monitor")
![monitor](http://image.jplogic.cn/img/show-grafana4.png "monitor")

**项目APM调用轨迹跟踪以及性能演示**

![monitor](http://image.jplogic.cn/img/show-skywalking1.png "monitor")
![monitor](http://image.jplogic.cn/img/show-skywalking2.png "monitor")
![monitor](http://image.jplogic.cn/img/show-skywalking3.png "monitor")

**项目截图演示**

搜索引擎
![搜索引擎](http://image.jplogic.cn/img/J2crawler_search.png "搜索引擎")
![搜索引擎](http://image.jplogic.cn/img/J2crawler_search5.png "搜索引擎")
![搜索引擎](http://image.jplogic.cn/img/J2crawler_search3.png "搜索引擎")
![搜索引擎](http://image.jplogic.cn/img/J2crawler_search2.png "搜索引擎")


日志控制台
![日志控制台](http://image.jplogic.cn/img/log-console.png "日志控制台")

角色管理-列表
![角色管理-列表](http://image.jplogic.cn/img/role-list.png "角色管理-列表")

角色管理-新增
![角色管理-新增](http://image.jplogic.cn/img/role-add.png "角色管理-新增")

部门管理-主页
![部门管理-主页.](http://image.jplogic.cn/img/dept1.png "部门管理-主页.")

菜单管理-主页
![菜单管理-主页](http://image.jplogic.cn/img/tree-mgr1.png "菜单管理-主页")

菜单管理-新增资源权限
![菜单管理-新增资源权限](http://image.jplogic.cn/img/tree-mgr1.png "菜单管理-新增资源权限")

菜单管理-权限资源设置
![菜单管理-权限资源设置](http://image.jplogic.cn/img/tree-mgr2.png "菜单管理-权限资源设置")

菜单管理-菜单设置
![菜单管理-菜单设置](http://image.jplogic.cn/img/tree-mgr3.png "菜单管理-菜单设置")

用户管理-列表
![用户管理-列表](http://image.jplogic.cn/img/user-list.png "用户管理-列表")

用户管理-登录ip
![用户管理-登录ip](http://image.jplogic.cn/img/user-login.png "用户管理-登录ip")

用户管理-编辑
![用户管理-编辑](http://image.jplogic.cn/img/user-edit.png "用户管理-编辑")

权限管理-主页
![权限管理-主页](http://image.jplogic.cn/img/pers.png "权限管理-主页")

日志管理-主页
![日志管理-主页](http://image.jplogic.cn/img/log-list.png "日志管理-主页")

布局设置
![布局设置](http://image.jplogic.cn/img/layout-setting.png "布局设置")

布局设置-上下结构
![布局设置-上下结构](http://image.jplogic.cn/img/layout-setting-up-down.png "布局设置-上下结构")

springbootadmin管理
![springbootadmin管理](http://image.jplogic.cn/img/springbootadmin.png "springbootadmin管理")

分布式api文档管理
![分布式api文档管理](http://image.jplogic.cn/img/swagger-api.png "分布式api文档管理")

分布式任务调度管理
![分布式任务调度管理](http://image.jplogic.cn/img/elastic-job.png "分布式任务调度管理")

分布式调用轨迹跟踪管理
![分布式调用轨迹跟踪管理](http://image.jplogic.cn/img/trace.png "分布式调用轨迹跟踪管理")

服务注册发现管理
![服务注册发现管理](http://image.jplogic.cn/img/register.png "服务注册发现管理")

服务熔断监控管理
![服务熔断监控管理](http://image.jplogic.cn/img/hystrix.png "服务熔断监控管理")

[![森之灵/jplogiccloud](https://gitee.com/romantichjw/jplogiccloud/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/romantichjw/jplogiccloud) 
